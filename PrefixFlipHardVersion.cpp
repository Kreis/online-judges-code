#include <iostream>
#include <vector>

int main(int argc, char** argv) {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);

  int32_t t, n;
  std::string a, b;

  std::vector<int32_t> moves;
  std::cin >> t;
  while (t-->0) {
    std::cin >> n >> a >> b;

    moves.clear();
    int32_t logicf = 0;
    int32_t logicl = n - 1;
    int32_t literall = n - 1;
    int32_t reversed = 0; 
    while (literall >= 0) {

      if ((a[ logicl ] ^ reversed) == b[ literall ]) {
        logicl += reversed ? 1 : -1;
        literall--;
        continue;
      }

      if (logicf == logicl) {
        moves.push_back(1);
        break;
      }
      
      if ((a[ logicf ] ^ reversed) == b[ literall ]) {
        moves.push_back(1);
      }

      moves.push_back(literall + 1);
      literall--;
      reversed ^= 1;
      std::swap(logicf, logicl);
      logicl += reversed ? 1 : -1;
    }

    std::cout << moves.size();
    for (int32_t i : moves) {
      std::cout << " " << i;
    }
    std::cout << std::endl;
  }

  return 0;
}

