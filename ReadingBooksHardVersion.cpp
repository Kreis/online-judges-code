#include <vector>
#include <bits/stdc++.h>
#include <iostream>
#define forby(i, a, z) for (int32_t (i) = a; i < z; i++)
#define vec std::vector<int32_t>
#define size(x) static_cast<int32_t>(x.size())
#define first(x) std::next(x.begin(), 0)
#define last(x) std::prev(x.end(), 1)
#define all(x) x.begin(), x.end()

int main(int argc, char** argv) {

  int32_t n, m, k;
  scanf("%d%d%d", &n, &m, &k);

  vec va;
  vec vb;
  vec vab;
  vec vall;
  vec vr;
 
  auto cmp = [&vall](int32_t p, int32_t q) {
    return vall[ p ] < vall[ q ];
  };

  std::multiset<int32_t, decltype(cmp)> setout(cmp);
  forby(i, 0, n) {
    int32_t t, a, b;
    scanf("%d%d%d", &t, &a, &b);

    vall.push_back(t);
    if (a && b) {
      vab.push_back(i);
    } else if (a) {
      va.push_back(i);
    } else if (b) {
      vb.push_back(i);
    } else {
      vr.push_back(i);
    }
  }
  
  std::sort(all(va), cmp);
  std::sort(all(vb), cmp);
  std::sort(all(vab), cmp);
  if (size(va) + size(vab) < k || size(vb) + size(vab) < k) {
    printf("%d\n", -1);
    return 0;
  } 

  int32_t incommon = std::min(size(vab), k);
  int32_t singular = k - incommon;
  
  if (singular > std::min(size(va), size(vb))) {
    printf("%d\n", -1);
    return 0;
  } 

  int32_t selected = (singular * 2) + incommon;
  if (selected > m) {
    printf("%d\n", -1);
    return 0;
  }

  std::multiset<int32_t, decltype(cmp)> setin(cmp);
  int32_t settime = 0; 
  auto balance = [&vall, &setin, &setout, &settime](int32_t n) {
    while (size(setin) < n) {
      auto f = first(setout);
      settime += vall[ *f ];
      setin.insert(*f);
      setout.erase(f);
    }
    while (size(setin) > n) {
      auto l = last(setin);
      settime -= vall[ *l ];
      setout.insert(*l);
      setin.erase(l);
    }
    while (size(setin) > 0 && size(setout) > 0 && vall[ *last(setin) ] > vall[ *first(setout) ]) {
      auto f = first(setout);
      auto l = last(setin);
      settime -= vall[ *l ];
      settime += vall[ *f ];
      setout.insert(*l);
      setin.insert(*f);
      setout.erase(f);
      setin.erase(l);
    }
  };
  
  int32_t ktime = 0;
  forby (i, 0, incommon) {
    ktime += vall[ vab[ i ] ];
  }
  forby(i, 0, singular) {
    ktime += vall[ va[ i ] ] + vall[ vb[ i ] ];
  }
  
  int32_t to_select = m - selected;
  forby (i, incommon, size(vab)) {
    setin.insert(vab[ i ]);
    settime += vall[ vab[ i ] ];
  }
  forby (i, singular, size(va)) {
    setin.insert(va[ i ]);
    settime += vall[ va[ i ] ];
  }
  forby (i, singular, size(vb)) {
    setin.insert(vb[ i ]);
    settime += vall[ vb[ i ] ];
  }
  forby (i, 0, size(vr)) {
    setout.insert(vr[ i ]);
  }
  balance(to_select);
  int32_t totaltime = ktime + settime;
  int32_t bestincommon = incommon;
  while (incommon > 0 && singular < std::min(size(va), size(vb)) && to_select > 0) {
    incommon--;
    ktime -= vall[ vab[ incommon ] ];
    setout.insert(vab[ incommon ]);

    auto setait = setin.find(va[ singular ]);
    if (setait == setin.end()) {
      setout.erase(setout.find(va[ singular ]));
    } else {
      setin.erase(setait);
      settime -= vall[ va[singular ] ];
    }
    auto setbit = setin.find(vb[ singular ]);
    if (setbit == setin.end()) {
      setout.erase(setout.find(vb[ singular ]));
    } else {
      setin.erase(setbit);
      settime -= vall[ vb[ singular ] ];
    }
    
    ktime += vall[ va[ singular ] ] + vall[ vb[ singular ] ];
    singular++;
    to_select--;

    balance(to_select);

    int32_t currenttime = ktime + settime;
    if (currenttime < totaltime) {
      totaltime = currenttime;
      bestincommon = incommon;
    }
  }

  int32_t bestsingular = k - bestincommon;
  int32_t bestremains = m - bestincommon - (bestsingular * 2);
  forby (i, bestincommon, size(vab)) {
    vr.push_back(vab[ i ]);
  }
  forby (i, bestsingular, size(va)) {
    vr.push_back(va[ i ]);
  }
  forby (i, bestsingular, size(vb)) {
    vr.push_back(vb[ i ]);
  }
  std::sort(all(vr), cmp);
  
  // answer
  printf("%d\n", totaltime);
  forby (i, 0, bestincommon) {
    printf("%d ", vab[ i ] + 1);
  }
  forby (i, 0, bestsingular) {
    printf("%d %d ", va[ i ] + 1, vb[ i ] + 1);
  }
  forby (i, 0, bestremains) {
    printf("%d ", vr[ i ] + 1);
  }
  printf("\n");

  return 0;
}

