#include <iostream>

int main(int argc, char** argv) {

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);

  int32_t t, n, a;
  std::cin >> t;
  while (t-- > 0) {
    std::cin >> n;

    int32_t many1 = 0;
    bool findit = false;
    while (n-- > 0) {
      std::cin >> a;
      if (findit) {
        continue;
      }
      if (a > 1) {
        findit = true;
      } else {
        many1 += a;
      }
    }

    bool firstWin = (many1 & 1) == findit ? 0 : 1;
    std::cout << (firstWin ? "First" : "Second") << std::endl;
  }

  return 0;
}
