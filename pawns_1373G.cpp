#include <vector>
#include <bits/stdc++.h>
#include <map>

// https://codeforces.com/problemset/problem/1373/G
//
class SegmentTree {
public:
  SegmentTree(int32_t n) {
    this->data.resize(n << 2 | 1, 0);
    this->u.resize(n << 2 | 1, 0);
    this->n = n;
  }

  void update(int32_t l, int32_t r, int32_t v) {
    this->update(l, r, v, 1, 1, this->n);
  }

  void update(int32_t l, int32_t r, int32_t v, int32_t nid, int32_t tl, int32_t tr) {

    if (l <= tl && r >= tr) {
      this->data[ nid ] += v;
      this->u[ nid ] += v;
      return;
    }

    if (l > tr || r < tl) {
      return;
    }

    int32_t tm = (tl + tr) >> 1;
    this->push(nid);
    this->update(l, r, v, nid << 1, tl, tm);
    this->update(l, r, v, nid << 1 | 1, tm + 1, tr);
    this->data[ nid ] = std::max(
      this->data[ nid << 1 ],
      this->data[ nid << 1 | 1 ]
    );
  }

  int32_t get(int32_t l, int32_t r) {
    return this->get(l, r, 1, 1, this->n);
  }

  int32_t get(int32_t l, int32_t r, int32_t nid, int32_t tl, int32_t tr) {

    if (l <= tl && r >= tr) {
      return this->data[ nid ];
    }

    if (l > tr || r < tl) {
      return -1;
    }

    int32_t tm = (tl + tr) >> 1;
    this->push(nid);
    return std::max(
      this->get(l, r, nid << 1, tl, tm),
      this->get(l, r, nid << 1 | 1, tm + 1, tr)
    );
  }

private:
  std::vector<int32_t> u;
  std::vector<int32_t> data;
  int32_t n;

  void push(int32_t nid) {
    if (this->u[ nid ]) {
      this->u[ nid << 1 ] += this->u[ nid ];
      this->u[ nid << 1 | 1 ] += this->u[ nid ];
      this->data[ nid << 1 ] += this->u[ nid ];
      this->data[ nid << 1 | 1 ] += this->u[ nid ];
      this->u[ nid ] = 0;
    }
  }
};

int main(int argc, char** argv) {
  std::multiset<int32_t> added;
  std::map<std::pair<int32_t, int32_t>, bool> memset;

  int32_t n, k, m;
  scanf("%d%d%d", &n, &k, &m);
  SegmentTree st(n + n);

  for (int32_t i = 1; i <= n + n; i++) {
    st.update(i, i, i - 1);
  }

  while (m-->0) {
    int32_t x, y;
    scanf("%d%d", &x, &y);
    int32_t pos = y + std::abs(x - k);
    if ( ! memset[{ x, y }]) {
      memset[{ x, y }] = true;
      st.update(1, pos, 1);
      added.insert(pos);
    } else {
      memset[{ x, y }] = false;
      st.update(1, pos, -1);
      added.erase(added.find(pos));
    }

    int32_t result = 0;
    if (added.size() > 0) {
      result = std::max(0, st.get(1, *added.rbegin()) - n);
    }
    
    printf("%d\n", result);
  }

  exit(0);
}
