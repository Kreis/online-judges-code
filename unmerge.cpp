#include <iostream>
#include <vector>
#include <algorithm>

int main(int argc, char** argv) {

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);

  int32_t t, n;
  std::cin >> t;
  while (t-- >0) {
    
    std::cin >> n;

    std::vector<int32_t> lengths;
    std::vector<int32_t> vec_a(n * 2 + 1);
    for (int32_t i = 0; i < n * 2; i++) {
      std::cin >> vec_a[ i ];
    }
    vec_a[ n * 2 ] = n * 2 + 1;

    int32_t current_max = vec_a[ 0 ];
    int32_t count = 1;
    for (int32_t i = 1; i < vec_a.size(); i++) {
      if (vec_a[ i ] < current_max) {
        count++;
        continue;
      }

      current_max = vec_a[ i ];
      lengths.push_back(count);
      count = 1;
    }

    // setsum problem
    std::vector<std::vector<bool> > setsum(n + 1, std::vector<bool>(2, false));
    int32_t up = 0;
    int32_t down = 1;
    setsum[ 0 ][ up ] = true;
    setsum[ 0 ][ down ] = true;

    std::sort(lengths.begin(), lengths.end());
    for (int32_t i = 0; i < lengths.size(); i++) {
      for (int32_t j = 1; j < setsum.size(); j++) {
        if (setsum[ j ][ up ] || lengths[ i ] > j) {
          setsum[ j ][ down ] = setsum[ j ][ up ];
          continue;
        }

        setsum[ j ][ down ] = setsum[ j - lengths[ i ] ][ up ];
      }
      std::swap(up, down);
    }
    
    std::cout << (setsum[ n ][ up ] ? "YES" : "NO") << std::endl;
  }

  return 0;
}

