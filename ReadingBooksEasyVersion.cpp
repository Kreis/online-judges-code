#include <iostream>
#include <vector>
#include <algorithm>
#define all(x) x.begin(), x.end()
#define vec std::vector<int32_t>

int32_t calcule(vec& vec_a, vec& vec_b, vec& vec_ab, int32_t k, int32_t incommon) {
  int32_t last_singular_id = k - 1 - incommon;
  return vec_ab[ incommon ] - vec_a[ last_singular_id ] - vec_b[ last_singular_id ];
}

int main(int argc, char** argv) {

  int32_t n, k;
  scanf("%d%d", &n, &k);

  vec vec_a;
  vec vec_b;
  vec vec_ab;

  for (int32_t i = 0; i < n; i++) {
    int32_t t, a, b;
    scanf("%d%d%d", &t, &a, &b);

    if (a == 1 && b == 1) {
      vec_ab.push_back(t);
    } else if (a == 1) {
      vec_a.push_back(t);
    } else if (b == 1) {
      vec_b.push_back(t);
    }
  }

  // check if impossible
  if (
      vec_a.size() + vec_ab.size() < k ||
      vec_b.size() + vec_ab.size() < k
     ) {
    printf("%d\n", -1);
    return 0;
  }

  std::sort(all(vec_a));
  std::sort(all(vec_b));
  std::sort(all(vec_ab));

  int32_t incommon = 0;
  // initial state
  int32_t total_time = 0;
  if (vec_a.size() >= k && vec_b.size() >= k) {
    for (int32_t i = 0; i < k; i++) {
      total_time += vec_a[ i ] + vec_b[ i ];
    }
  } else {
    int32_t min = std::min(vec_a.size(), vec_b.size());
    for (int32_t i = 0; i < min; i++) {
      total_time += vec_a[ i ] + vec_b[ i ];
    }    
    for (int32_t i = 0; i < k - min; i++) {
      total_time += vec_ab[ i ];
    }
    incommon = k - min;
  }

  while (incommon < k && incommon < vec_ab.size()) {
    int32_t current_time = total_time + calcule(vec_a, vec_b, vec_ab, k, incommon++);
    if (current_time >= total_time) {
      break;
    }

    total_time = current_time;
  }

  printf("%d\n", total_time);

  return 0;
}

