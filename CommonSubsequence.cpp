#include <iostream>
#include <vector>

int main(int argc, char** argv) {

  std::ios_base::sync_with_stdio(false);
  std::cin.tie(NULL);

  std::vector<int32_t> vec(1001, -1);

  int32_t t, n, m, num;
  std::cin >> t;
  while (t-->0) {
    std::cin >> n >> m;
    bool foundit = false;
    for (int32_t i = 0; i < n; i++) {
      std::cin >> num;
      vec[ num ] = t;
    }
    for (int32_t i = 0; i < m; i++) {
      std::cin >> num;
      if ( ! foundit && vec[ num ] == t) {
        std::cout << "YES" << std::endl << "1 " << num << std::endl;
        foundit = true;
      }
    }

    if ( ! foundit) {
      std::cout << "NO" << std::endl;
    }

  }
 
  return 0;
}
